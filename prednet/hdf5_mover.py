import h5py, numpy as np

h5file1 = h5py.File('./prednet_kitti_weights.hdf5', 'w')
print (h5file1)

# Remove top layer
layer_a_0 = h5file1['/prednet_1/prednet_1/layer_a_0']
layer_a_1 = h5file1['/prednet_1/prednet_1/layer_a_1']
layer_ahat_0 = h5file1['/prednet_1/prednet_1/layer_ahat_0']
layer_ahat_1 = h5file1['/prednet_1/prednet_1/layer_ahat_1']
layer_ahat_2 = h5file1['/prednet_1/prednet_1/layer_ahat_2']
layer_c_0 = h5file1['/prednet_1/prednet_1/layer_c_0']
layer_c_1 = h5file1['/prednet_1/prednet_1/layer_c_1']
layer_c_2 = h5file1['/prednet_1/prednet_1/layer_c_2']
layer_f_0 = h5file1['/prednet_1/prednet_1/layer_f_0']
layer_f_1 = h5file1['/prednet_1/prednet_1/layer_f_1']
layer_f_2 = h5file1['/prednet_1/prednet_1/layer_f_2']
layer_i_0 = h5file1['/prednet_1/prednet_1/layer_i_0']
layer_i_1 = h5file1['/prednet_1/prednet_1/layer_i_1']
layer_i_2 = h5file1['/prednet_1/prednet_1/layer_i_2']
layer_o_0 = h5file1['/prednet_1/prednet_1/layer_o_0']
layer_o_1 = h5file1['/prednet_1/prednet_1/layer_o_1']
layer_o_2 = h5file1['/prednet_1/prednet_1/layer_o_2']

layer_dense_2 = h5file1['/dense_2']
layer_flatten_1 = h5file1['/flatten_1']
layer_input_1 = h5file1['/input_1']
layer_timedistributed_1 = h5file1['/timedistributed_1']

# Copy to new file
h5file2 = h5py.File('./model_data_keras2/tensorflow_weights/prednet_kitti_weights_3_layers.hdf5', 'w')
h5file2.create_dataset('/prednet_1/prednet_1/layer_a_0', data=layer_a_0)
h5file2.create_dataset('/prednet_1/prednet_1/layer_a_1', data=layer_a_1)
h5file2.create_dataset('/prednet_1/prednet_1/layer_ahat_0', data=layer_ahat_0)
h5file2.create_dataset('/prednet_1/prednet_1/layer_ahat_1', data=layer_ahat_1)
h5file2.create_dataset('/prednet_1/prednet_1/layer_ahat_2', data=layer_ahat_2)
h5file2.create_dataset('/prednet_1/prednet_1/layer_c_0', data=layer_c_0)
h5file2.create_dataset('/prednet_1/prednet_1/layer_c_1', data=layer_c_1)
h5file2.create_dataset('/prednet_1/prednet_1/layer_c_2', data=layer_c_2)
h5file2.create_dataset('/prednet_1/prednet_1/layer_f_0', data=layer_f_0)
h5file2.create_dataset('/prednet_1/prednet_1/layer_f_1', data=layer_f_1)
h5file2.create_dataset('/prednet_1/prednet_1/layer_f_2', data=layer_f_2)
h5file2.create_dataset('/prednet_1/prednet_1/layer_i_0', data=layer_i_0)
h5file2.create_dataset('/prednet_1/prednet_1/layer_i_1', data=layer_i_1)
h5file2.create_dataset('/prednet_1/prednet_1/layer_i_2', data=layer_i_2)
h5file2.create_dataset('/prednet_1/prednet_1/layer_o_0', data=layer_o_0)
h5file2.create_dataset('/prednet_1/prednet_1/layer_o_1', data=layer_o_1)
h5file2.create_dataset('/prednet_1/prednet_1/layer_o_2', data=layer_o_2)
# print (h5file2)

h5file1.close()
h5file2.close()