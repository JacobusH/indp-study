import glob
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import imageio as im
import os
import pathlib
from keras import models
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint

def plot_training_set():
    ### To show imgs/plots
    # Circles
    images = []
    for img_path in glob.glob('./training_set/circles/*.png'):
        images.append(mpimg.imread(img_path))
    plt.figure(figsize=(20,10))
    columns = 5
    for i, image in enumerate(images):
        plt.subplot(len(images) / columns + 1, columns, i + 1)
        plt.imshow(image)
    # Squares
    images = []
    for img_path in glob.glob('training_set/squares/*.png'):
        images.append(mpimg.imread(img_path))
    plt.figure(figsize=(20,10))
    columns = 5
    for i, image in enumerate(images):
        plt.subplot(len(images) / columns + 1, columns, i + 1)
        plt.imshow(image)
    # Triangles
    images = []
    for img_path in glob.glob('training_set/triangles/*.png'):
        images.append(mpimg.imread(img_path))
    plt.figure(figsize=(20,10))
    columns = 5
    for i, image in enumerate(images):
        plt.subplot(len(images) / columns + 1, columns, i + 1)
        plt.imshow(image)
    plt.show()