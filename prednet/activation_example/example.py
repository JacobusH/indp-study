import glob
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import imageio as im
import os
import pathlib
from keras import models
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint

from plotAllChannels import plot_all
from plotTrainingSet import plot_training_set

### Example from:
# https://towardsdatascience.com/visualizing-intermediate-activation-in-convolutional-neural-networks-with-keras-260b36d60d0

checkpoint_path = "best_weights.ckpt" # normal path
if "activation_example" not in os.path.dirname(os.path.dirname(__file__)) and os.path.dirname(os.path.dirname(__file__)) != "": # when debugging
    os.chdir(os.getcwd()+"/prednet/activation_example")

### To plot training set
# plot_training_set()

# Initialising the CNN
classifier = Sequential()

# Step 1 - Convolution
classifier.add(Conv2D(32, (3, 3), padding='same', input_shape = (28, 28, 3), activation = 'relu'))
classifier.add(Conv2D(32, (3, 3), activation='relu'))
classifier.add(MaxPooling2D(pool_size=(2, 2)))
classifier.add(Dropout(0.5)) # antes era 0.25
# Adding a second convolutional layer
classifier.add(Conv2D(64, (3, 3), padding='same', activation = 'relu'))
classifier.add(Conv2D(64, (3, 3), activation='relu'))
classifier.add(MaxPooling2D(pool_size=(2, 2)))
classifier.add(Dropout(0.5)) # antes era 0.25
# Adding a third convolutional layer
classifier.add(Conv2D(64, (3, 3), padding='same', activation = 'relu'))
classifier.add(Conv2D(64, (3, 3), activation='relu'))
classifier.add(MaxPooling2D(pool_size=(2, 2)))
classifier.add(Dropout(0.5)) # antes era 0.25
# Step 3 - Flattening
classifier.add(Flatten())# Step 4 - Full connection
classifier.add(Dense(units = 512, activation = 'relu'))
classifier.add(Dropout(0.5)) 
classifier.add(Dense(units = 3, activation = 'softmax'))
# classifier.summary()classifier.summary()

# Compiling the CNN
# classifier.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])
# train_datagen = ImageDataGenerator(rescale = 1./255)
# test_datagen  = ImageDataGenerator(rescale = 1./255) 
# training_set  = train_datagen.flow_from_directory('training_set', target_size = (28,28), batch_size = 16, class_mode ='categorical')
# test_set      = test_datagen.flow_from_directory('test_set', target_size = (28, 28), batch_size = 16, class_mode ='categorical')

# cp_callback     = ModelCheckpoint(filepath=checkpoint_path, save_weights_only=True, save_best_only=True, verbose=1)
# history         = classifier.fit_generator(training_set, steps_per_epoch=100, epochs=12, callbacks=[cp_callback], validation_data=test_set, validation_steps=50)
# # print(checkpoint_path)

classifier.load_weights(checkpoint_path)
classifier.save('shapes_cnn.h5')

### Training inspection
# acc = history.history['acc']
# val_acc = history.history['val_acc']
# loss = history.history['loss']
# val_loss = history.history['val_loss']
# epochs = range(1, len(acc) + 1)
# plt.plot(epochs, acc, 'bo', label='Training acc')
# plt.plot(epochs, val_acc, 'b', label='Validation acc')
# plt.title('Training and validation accuracy')
# plt.legend()
# plt.figure()
# plt.plot(epochs, loss, 'bo', label='Training loss')
# plt.plot(epochs, val_loss, 'b', label='Validation loss')
# plt.title('Training and validation loss')
# plt.legend()
# plt.show()

### Predictions
img_path = 'test_set/triangles/drawing(2).png'
img = image.load_img(img_path, target_size=(28, 28))
img_tensor = image.img_to_array(img)
img_tensor = np.expand_dims(img_tensor, axis=0)
img_tensor /= 255.
# plt.imshow(img_tensor[0])
# plt.show()
# print(img_tensor.shape)

# predicting images
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
images = np.vstack([x])
classes = classifier.predict_classes(images, batch_size=10)
# print("Predicted class is:",classes)

### Getting intermediate activation
layer_outputs    = [layer.output for layer in classifier.layers[:12]] 
activation_model = models.Model(inputs=classifier.input, outputs=layer_outputs)

activations = activation_model.predict(img_tensor) 

### Plot single activation channel
first_layer_activation = activations[0]
# print(first_layer_activation.shape)
plt.matshow(first_layer_activation[0, :, :, 4], cmap='viridis') # plot 4th channel activation (of the 32)
plt.show()

### Plot all activations of channels
# plot_all(classifier, activations, 15)
