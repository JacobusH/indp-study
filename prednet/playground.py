import tensorflow as tf
import datetime
import os

checkpoint_path = "training_1/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

def create_model():
  return tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28), name="layer_inp"),
    tf.keras.layers.Dense(512, activation='relu', name="layer_dense1"),
    tf.keras.layers.Dropout(0.2, name="layer_dropouts"),
    tf.keras.layers.Dense(10, activation='softmax', name="layer_softmax")
  ])

def train_model():
    model = create_model()
    model.summary()
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    # tensorboard data
    log_dir="logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
                
    # Create a callback that saves the model's weights
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path, save_weights_only=True, verbose=1)
    # Train the model with the new checkpoint callback
    model.fit(x=x_train,  y=y_train, epochs=5, validation_data=(x_test,y_test), callbacks=[cp_callback, tensorboard_callback])  
    # Save the weights
    model.save_weights('./checkpoints/my_checkpoint')

def load_model():
    model = create_model()
    latest = tf.train.latest_checkpoint(checkpoint_dir)
    model.load_weights(latest)
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    # loss, acc = model.evaluate(x_test,  y_test, verbose=2)
    # print("Restored model, accuracy: {:5.2f}%".format(100*acc))
    return model

### Get test data
mnist = tf.keras.datasets.mnist
(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

### Train model and save its values
# train_model()

### Load model from trained
model = load_model()
# model.predict()

### Get names of all tensors in graph
# [print(n.name) for n in tf.get_default_graph().as_graph_def().node]

######
### TRYING TO GET ACTIVATIONS DURING PREDICTION
######
# https://github.com/tensorflow/tensorflow/issues/33478
# - "The input and output properties simply track the tensors created when the model built, not the tensors created during the predictions"

intermediate_1 = model.get_layer("layer_dense1").output
tmp = "tmp"




# graph = tf.get_default_graph()
# list_of_tuples = [op.values() for op in graph.get_operations()]





#


# layer_name = 'my_layer'
# intermediate_layer_model = Model(inputs=model.input, outputs=model.get_layer(layer_name).output)
# intermediate_output = intermediate_layer_model.predict(data)