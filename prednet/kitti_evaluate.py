'''
Evaluate trained PredNet on KITTI sequences.
Calculates mean-squared error and plots predictions.
'''

import os
import numpy as np
import copy
from six.moves import cPickle
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from keras import backend as K
from keras.models import Model, model_from_json
from keras.layers import Input, Dense, Flatten

from prednet import PredNet
from data_utils import SequenceGenerator
from kitti_settings import *


# check correct working directory (for debugging)
if "prednet" not in os.getcwd():
    os.chdir(os.getcwd()+"/prednet")

run_as = 'prediction' # prediction | error

n_plot = 40
batch_size = 10
nt = 10

weights_file = os.path.join(WEIGHTS_DIR, 'tensorflow_weights/prednet_kitti_weights.hdf5')
json_file = os.path.join(WEIGHTS_DIR, 'prednet_kitti_model.json')
test_file = os.path.join(DATA_DIR, 'X_test.hkl')
test_sources = os.path.join(DATA_DIR, 'sources_test.hkl')

# Load trained model
f = open(json_file, 'r')
json_string = f.read()
f.close()
train_model = model_from_json(json_string, custom_objects = {'PredNet': PredNet})
train_model.load_weights(weights_file)

# Create testing model (to output predictions)
layer_config = train_model.layers[1].get_config()
if run_as == 'prediction': layer_config['output_mode'] = 'prediction'
if run_as == 'error': layer_config['output_mode'] = 'error'
data_format = layer_config['data_format'] if 'data_format' in layer_config else layer_config['dim_ordering']

test_prednet = PredNet(weights=train_model.layers[1].get_weights(), **layer_config)

input_shape = list(train_model.layers[0].batch_input_shape[1:])
input_shape[0] += nt
inputs = Input(shape=tuple(input_shape))
predictions = test_prednet(inputs) # after this: creates all tensors for conv_layers


### Trying to remove top prednet layer
# new weights
# weights_all = train_model.layers[1].get_weights()
# train_model.layers[1].conv_layers['a'] = train_model.layers[1].conv_layers['a'][:-1]
# train_model.layers[1].conv_layers['ahat'] = train_model.layers[1].conv_layers['ahat'][:-1]
# train_model.layers[1].conv_layers['c'] = train_model.layers[1].conv_layers['c'][:-1]
# train_model.layers[1].conv_layers['f'] = train_model.layers[1].conv_layers['f'][:-1]
# train_model.layers[1].conv_layers['i'] = train_model.layers[1].conv_layers['i'][:-1]
# train_model.layers[1].conv_layers['o'] = train_model.layers[1].conv_layers['o'][:-1]
# tmp_weights = train_model.layers[1].get_weights()

### This doesn't seem to get the actual weights... just the layers and their shape (incorrectly)
weights_3l = []
weights_2l = []
weights_1l = []
for lay_weight in train_model.layers[1].trainable_weights:
    if "_3/" not in lay_weight.name and "a_2" not in lay_weight.name: # removes layer 4
        weights_3l.append(lay_weight)
    if "_3/" not in lay_weight.name and "a_2" not in lay_weight.name and "_2/" not in lay_weight.name and "a_1" not in lay_weight.name: # removes layer 3 
        weights_2l.append(lay_weight)
    if "_3/" not in lay_weight.name and "a_2" not in lay_weight.name and "_2/" not in lay_weight.name and "a_1" not in lay_weight.name and "_1/k" not in lay_weight.name and "_1/b" not in lay_weight.name and "a_0" not in lay_weight.name:
        weights_1l.append(lay_weight)

### Try to get weights individually
# weights_1l = []
# weights_1l.append(train_model.layers[1].conv_layers['a'][0].get_weights()) # no a for first layer?
# weights_1l.append(train_model.layers[1].conv_layers['ahat'][0].get_weights()[0]) # kernel
# weights_1l.append(train_model.layers[1].conv_layers['ahat'][0].get_weights()[1]) # bias
# weights_1l.append(train_model.layers[1].conv_layers['c'][0].get_weights()[0])
# weights_1l.append(train_model.layers[1].conv_layers['c'][0].get_weights()[1])
# weights_1l.append(train_model.layers[1].conv_layers['f'][0].get_weights()[0])
# weights_1l.append(train_model.layers[1].conv_layers['f'][0].get_weights()[1])
# weights_1l.append(train_model.layers[1].conv_layers['i'][0].get_weights()[0])
# weights_1l.append(train_model.layers[1].conv_layers['i'][0].get_weights()[1])
# weights_1l.append(train_model.layers[1].conv_layers['o'][0].get_weights()[0])
# weights_1l.append(train_model.layers[1].conv_layers['o'][0].get_weights()[1])

# new config
new_config_3l = copy.deepcopy(layer_config)
new_config_2l = copy.deepcopy(layer_config)
new_config_1l = copy.deepcopy(layer_config)

# new_config_3l['A_filt_sizes'] = new_config_3l['A_filt_sizes'][0:-1] 
# new_config_3l['Ahat_filt_sizes'] = new_config_3l['Ahat_filt_sizes'][0:-1] 
# new_config_3l['R_filt_sizes'] = new_config_3l['R_filt_sizes'][0:-1] 
# new_config_3l['R_stack_sizes'] = new_config_3l['R_stack_sizes'][0:-1] 
# new_config_3l['stack_sizes'] = new_config_3l['stack_sizes'][0:-1] 
# new_prednet_3l = PredNet(weights=weights_3l, **new_config_3l)

new_config_2l['A_filt_sizes'] = new_config_2l['A_filt_sizes'][0:-2] 
new_config_2l['Ahat_filt_sizes'] = new_config_2l['Ahat_filt_sizes'][0:-2] 
new_config_2l['R_filt_sizes'] = new_config_2l['R_filt_sizes'][0:-2] 
new_config_2l['R_stack_sizes'] = new_config_2l['R_stack_sizes'][0:-2] 
new_config_2l['stack_sizes'] = new_config_2l['stack_sizes'][0:-2] 
new_prednet_2l = PredNet(weights=weights_2l, **new_config_2l)

# new_config_1l['A_filt_sizes'] = new_config_1l['A_filt_sizes'][0:-3] 
# new_config_1l['Ahat_filt_sizes'] = new_config_1l['Ahat_filt_sizes'][0:-3] 
# new_config_1l['R_filt_sizes'] = new_config_1l['R_filt_sizes'][0:-3] 
# new_config_1l['R_stack_sizes'] = new_config_1l['R_stack_sizes'][0:-3] 
# new_config_1l['stack_sizes'] = new_config_1l['stack_sizes'][0:-3] 
# new_prednet_1l = PredNet(weights=weights_1l, **new_config_1l)

# new_predictions_3l = new_prednet_3l(inputs)
new_predictions_2l = new_prednet_2l(inputs)
# new_predictions_1l = new_prednet_1l(inputs)



test_model = Model(inputs=inputs, outputs=predictions)
tor = SequenceGenerator(test_file, test_sources, nt, sequence_start_mode='unique', data_format=data_format)
X_test = test_generator.create_all()
X_hat = test_model.predict(X_test, batch_size)

######
### Trying to get intermediate activations
######
# - how to get only one input from the 83 in sequenceGen?

### This just gives last layer output same as running full model
# model_last_layer = Model(inputs=inputs, outputs=test_model.layers[-1].output)
# acts_ll = model_last_layer.predict(X_test, 10) 

### Can't do this cause this layer is the input
# model_first_layer = Model(inputs=inputs, outputs=test_model.layers[0].output)
# acts_fl = model_first_layer.predict(X_test, 10) 

### Can't do this cause even tho the  model has been used the prednet parts are still not registered as connected...
# model_1 = Model(inputs=inputs, outputs=test_model.layers[-1].conv_layers['a'][0].output)
# acts_1 = model_1.predict(X_test, 10)

### This still doesn't have output or input, even after predictions go thru
# pred_out = test_prednet.conv_layers['a'][0].output

### loc of weights
# test_model.layers[-1].conv_layers['a'][0].weights[0] 


if data_format == 'channels_first':
    X_test = np.transpose(X_test, (0, 1, 3, 4, 2))
    if run_as == 'prediction': X_hat = np.transpose(X_hat, (0, 1, 3, 4, 2))
    # if run_as == 'error': X_hat = np.transpose(X_hat, (0, 1, 3))

# Compare MSE of PredNet predictions vs. using last frame.  Write results to prediction_scores.txt
mse_model = np.mean( (X_test[:, 1:] - X_hat[:, 1:])**2 )  # look at all timesteps except the first
mse_prev = np.mean( (X_test[:, :-1] - X_test[:, 1:])**2 )
if not os.path.exists(RESULTS_SAVE_DIR): os.mkdir(RESULTS_SAVE_DIR)
f = open(RESULTS_SAVE_DIR + 'prediction_scores.txt', 'w')
f.write("Model MSE: %f\n" % mse_model)
f.write("Previous Frame MSE: %f" % mse_prev)
f.close()

# Plot some predictions
aspect_ratio = float(X_hat.shape[2]) / X_hat.shape[3]
plt.figure(figsize = (nt, 2*aspect_ratio))
gs = gridspec.GridSpec(2, nt)
gs.update(wspace=0., hspace=0.)
plot_save_dir = os.path.join(RESULTS_SAVE_DIR, 'prediction_plots/')
if not os.path.exists(plot_save_dir): os.mkdir(plot_save_dir)
plot_idx = np.random.permutation(X_test.shape[0])[:n_plot]
for i in plot_idx:
    for t in range(nt):
        plt.subplot(gs[t])
        plt.imshow(X_test[i,t], interpolation='none')
        plt.tick_params(axis='both', which='both', bottom='off', top='off', left='off', right='off', labelbottom='off', labelleft='off')
        if t==0: plt.ylabel('Actual', fontsize=10)

        plt.subplot(gs[t + nt])
        plt.imshow(X_hat[i,t], interpolation='none')
        plt.tick_params(axis='both', which='both', bottom='off', top='off', left='off', right='off', labelbottom='off', labelleft='off')
        if t==0: plt.ylabel('Predicted', fontsize=10)

    plt.savefig(plot_save_dir +  'plot_' + str(i) + '.png')
    plt.clf()


######
### Trying to get activations
######
# https://towardsdatascience.com/visualizing-intermediate-activation-in-convolutional-neural-networks-with-keras-260b36d60d0
### Understanding tensors
# https://medium.com/@ecaradec/understanding-keras-tensors-d405a63e069e

# plt.matshow(X_hat[0][0][:,:,0], cmap='viridis')
# plt.show()

# test_prednet
# prednet_obj = train_model.layers[1] 
# prednet_layers = prednet_obj.conv_layers
# --- layer_c: input layer of LSTM
# --- layer_f: LSTM inner layer 1
# --- layer_i: LSTM inne