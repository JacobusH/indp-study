import sys
import math
import random
import numpy as np

def newDotPattern(ndots, imgLenPx, dotSize, recheckDist): 
    infCounter      = 0
    recheckCounter  = 1000
    while recheckCounter == 1000:
        for curdot in range(0, ndots):
            # Dot position, x,y
            tempDotPattern = [random.uniform(0.1, 0.9)*imgLenPx, random.uniform(0.1, 0.9)*imgLenPx]

            # Get dot pattern that works for img size
            while math.sqrt((tempDotPattern[0]-0.5*imgLenPx)**2+(tempDotPattern[1]-0.5*imgLenPx)**2) > 0.5*imgLenPx-dotSize/2:
                tempDotPattern  = [random.uniform(0.1, 0.9)*imgLenPx, random.uniform(0.1, 0.9)*imgLenPx]
                infCounter      = infCounter + 1
                if infCounter >= 5000:
                    sys.exit("fatal error....")
            A = tempDotPattern[0]
            B = tempDotPattern[1]

            if curdot == 0:
                dotGroup        = (tempDotPattern,) # make initial tuple
                recheckCounter  = 1
            else:
                recheck         = 1
                recheckCounter  = 1
                while recheck == 1:
                    recheck = 0
                    for storedDots in range(0, len(dotGroup)):
                        if recheck == 0:
                            xDist       = dotGroup[storedDots][0]-A
                            yDist       = dotGroup[storedDots][1]-B
                            totalDist   = math.sqrt(xDist**2 + yDist**2)
                            if totalDist < (dotSize * recheckDist):
                                recheck = 1
                    
                    if recheck == 0:
                        dotGroup = (*dotGroup, [A, B])
                    else:
                        tempDotPattern = [random.uniform(0.1, 0.9)*imgLenPx, random.uniform(0.1, 0.9)*imgLenPx]
                        while math.sqrt((tempDotPattern[0]-0.5*imgLenPx)**2+(tempDotPattern[1]-0.5*imgLenPx)**2)>0.5*imgLenPx-dotSize/2:
                            tempDotPattern  = [random.uniform(0.1, 0.9)*imgLenPx, random.uniform(0.1, 0.9)*imgLenPx]
                        A = tempDotPattern[0]
                        B = tempDotPattern[1]
                        recheckCounter = recheckCounter+1
                        if recheckCounter == 1000:
                            dotGroup = dotGroup[:-1:]
                            sys.exit("rechecks exceed maximum, double check parameter tuning")
                            break
    return dotGroup
                        

                


