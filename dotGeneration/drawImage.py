from PIL import Image, ImageDraw


def drawImage(imgWidth, dotGroup, dotSize): # image is always square
    # imgWidth: height and width of png base img
    # dotGroup: tuple of x,y coords of circle ([1,39], [43,2], ...)
    # dotSize: diameter of a single dot
    
    # make a blank image first
    image   = Image.new('RGB', (imgWidth, imgWidth))
    draw    = ImageDraw.Draw(image)

    r = dotSize / 2
    for dot in dotGroup:
        draw.ellipse((dot[0]-r, dot[1]-r, dot[0]+r, dot[1]+r), fill=(255,255,255))

    image.save("image.png", "PNG")